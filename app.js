"use strict";

document.addEventListener('DOMContentLoaded',function() {
const feature = document.querySelectorAll('.feature');
for (const iterator of feature) {
    iterator.style.textAlign = 'center'

}
console.log(feature)
})

document.addEventListener('DOMContentLoaded', function() {
    const h2Elements = document.querySelectorAll('h2');
    for (const el of h2Elements) {
        el.innerText = 'Awesome feature'
    }
    console.log(h2Elements);
});

document.addEventListener('DOMContentLoaded', function() {
    const featureTitle = document.querySelectorAll('.feature-title');

for (const i of featureTitle) {
    i.innerText += '!'

}
console.log(featureTitle)
})

document.addEventListener('DOMContentLoaded', function() {

    const createElement = document.createElement('a');
    createElement.textContent = 'Learn More';
    createElement.setAttribute('href', '#');
    const foot = document.querySelector('footer');
    foot.insertBefore(createElement, foot.children[1]);
});

document.addEventListener("DOMContentLoaded", function () {
  const sel = document.createElement("select");
  sel.id = "rating";

  const main = document.querySelector("main");

  main.prepend(sel);

  let option4 = document.createElement("option");
  option4.value = "4";
  option4.textContent = "4 Stars";
  sel.appendChild(option4);

  let option3 = document.createElement("option");
  option3.value = "3";
  option3.textContent = "3 Stars";
  sel.appendChild(option3);

  let option2 = document.createElement("option");
  option2.value = "2";
  option2.textContent = "2 Stars";
  sel.appendChild(option2);

  let option1 = document.createElement("option");
  option1.value = "1";
  option1.textContent = "1 Star";
  sel.appendChild(option1);
});
